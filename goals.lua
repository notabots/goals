local moduleInfo = {
	name = "goals",
	desc = "module providing goalTypes currently",
	author = "PepeAmpere",
	date = "2018-02-09",
	license = "MIT",
}


local goalTypes = {
	["captureFlags"] = {
		currentFlags = 0,
		flagsToWin = 0,
	
		Value = function(p)
			return p.currentFlags >= p.flagsToWin
		end,
		Tooltip = function(p)
			if (p.Value(p)) then
				return "Goal is completed."
			else
				return "Goal is not completed."
			end
		end,
		Counter = function(p)
			return p.currentFlags .. " / " .. p.flagsToWin	
		end,
		Description = function(p)
			return "Capture " .. p.flagsToWin .. " flags."
		end,
	},
	
	["destroyEnemyUnits"] = {
		unitsText = "",
		unitsDestroyed = -1,
		unitsAll = 0,
		allianceID = -1,
	
		Value = function(p)
			return p.unitsDestroyed == p.unitsAll
		end,
		Tooltip = function(p)
			if (p.Value(p)) then
				return "Goal is completed."
			else
				return "Goal is not completed."
			end
		end,
		Counter = function(p)
			local difference = p.unitsAll - p.unitsDestroyed
			local THRESHOLD = 10
			if difference > THRESHOLD or p.unitsAll > THRESHOLD then
				return difference .. ""
			else
				return p.unitsDestroyed .. "/" .. p.unitsAll
			end
		end,
		Description = function(p)
			return "Destroy " .. p.unitsText .. " of enemy alliance " .. p.allianceID .. "."
		end,
	},
		
	["holdFlags"] = {
		remainingTime = hmsf(0,0,0,0),
		totalTime = hmsf(0,0,0,0),
		totalTimeText = "0 minutes",
		flagsToWin = 0,
	
		Value = function(p)
			return p.remainingTime <= hmsf(0,0,0,0)
		end,
		Tooltip = function(p)
			if (p.Value(p)) then
				return "Goal is completed."
			else
				return "Goal is not completed."
			end
		end,
		Counter = function(p)
			return p.remainingTime:HHMMSSFF(false,true,true,false)	
		end,
		Description = function(p)
			return "Hold " .. p.flagsToWin .. " flags for " .. p.totalTimeText .. "."
		end,
	},
	
	["holdArea"] = {
		occupied = false,
		topLeft = 0,
		topRight = 0,
		bottomLeft = 0,
		bottoRight = 0,
	
		Value = function(p)
			return p.occupied
		end,
		Tooltip = function(p)
			if (p.Value(p)) then
				return "Area is completed."
			else
				return "Area is not completed."
			end
		end,
		Counter = function(p)
			if (p.Value(p)) then
				return "Taken"
			else
				return "X"
			end
		end,
		Description = function(p)
			return "Capture area."
		end,
	},
	
	["preventCapturingFlags"] = {
		currentFlags = 0,
		flagsToWin = 0,
		allianceID = -1,
	
		Value = function(p)
			return p.currentFlags < p.flagsToWin
		end,
		Tooltip = function(p)
			if (p.Value(p)) then
				return "Goal is completed."
			else
				return "Goal is not completed."
			end
		end,
		Counter = function(p)
			return p.currentFlags .. " / " .. p.flagsToWin	
		end,
		Description = function(p)
			return "Prevent alliance " .. p.allianceID .. " from capturing " .. p.flagsToWin .. " flags."
		end,
	},
	
	["preventHoldingFlags"] = {
		remainingTime = hmsf(0,0,0,0),
		totalTime = hmsf(0,0,0,0),
		totalTimeText = "0 minutes",
		flagsToWin = 0,
		allianceID = -1,
	
		Value = function(p)
			return p.remainingTime > hmsf(0,0,0,0)
		end,
		Tooltip = function(p)
			if (p.Value(p)) then
				return "Goal is completed."
			else
				return "Goal is not completed."
			end
		end,
		Counter = function(p)
			return p.remainingTime:HHMMSSFF(false,true,true,false)
		end,
		Description = function(p)
			return "Prevent alliance " .. p.allianceID .. " from holding " .. p.flagsToWin .. " flags for " .. p.totalTimeText .. "."
		end,
	},
	
	["preventDestroyingUnits"] = {
		unitsText = "",
		unitsDestroyed = 0,
		unitsAll = 0,
	
		Value = function(p)
			return p.unitsDestroyed ~= p.unitsAll
		end,
		Tooltip = function(p)
			if (p.Value(p)) then
				return "Goal is completed."
			else
				return "Goal is not completed."
			end
		end,
		Counter = function(p)
			return p.unitsDestroyed .. "/" .. p.unitsAll
		end,
		Description = function(p)
			return "Prevent destroying of your " .. p.unitsText .. "."
		end,
	},
}

return goalTypes

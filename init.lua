-- universal load for goals module
-- if you want customize it for your game, load your configs BEFORE including this file

local MODULE_NAME = "goals"
Spring.Echo("-- " .. MODULE_NAME .. " LOADING --")

------------------------------------------------------

-- MANDATORY
-- check required modules
if (modules == nil) then Spring.Echo("[" .. MODULE_NAME .. "] ERROR: required madatory config [modules] listing paths for modules is missing") end
if (attach == nil) then Spring.Echo("[" .. MODULE_NAME .. "] ERROR: required madatory library [attach] for loading files and modules is missing") end
if (message == nil) then Spring.Echo("[" .. MODULE_NAME .. "] ERROR: required madatory module [message] for communication is missing") end
if (hmsf == nil) then Spring.Echo("[" .. MODULE_NAME .. "] ERROR: required madatory module [hmsf] for communication is missing") end

------------------------------------------------------

local thisModuleData = modules[MODULE_NAME]
local THIS_MODULE_DATA_PATH = thisModuleData.data.path

-- LOAD GOALS LIBRARY SAME WAY IN ANY CONTEXT
local goalTypes = attach.File(THIS_MODULE_DATA_PATH .. "goals.lua")

local THIS_MODULE_CONFIG_PATH = thisModuleData.config.path
local listOfFiles = thisModuleData.config.files
for i=1, #listOfFiles do
	local filePath = THIS_MODULE_CONFIG_PATH .. listOfFiles[i]
	local newGoals = attach.try.File(filePath, "goals")
	if (newGoals) then		
		for k,v in pairs(newGoals) do
			goalTypes[k] = v
		end
	end
end

------------------------------------------------------

Spring.Echo("-- " .. MODULE_NAME .. " LOADING FINISHED --")

return goalTypes